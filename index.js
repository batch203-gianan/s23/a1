//alert("Welcome, Trainer!");

// let trainer = {
// 	name: "Ash Ketchup",
// 	age: 10,
// 	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
// 	friends: {
// 		hoenn: ["May", "Max"],
// 		kanto: ["Brock", "Misty"]
// 	},
// 	talk: function(pokemon){
// 		console.log(this.pokemon[pokemon] + " I choose you!");
// 	}
// };

let trainer = {};

trainer.name = "Ash Ketchup";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
 		hoenn: ["May", "Max"],
 		kanto: ["Brock", "Misty"]
 	};

trainer.talk = function(pokemon){
 		console.log(this.pokemon[pokemon] + " I choose you!");
 	};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk(0);

//end